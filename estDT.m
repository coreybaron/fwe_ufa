function [MD,FA,eigVals,DT] = estDT(sigVals, bVals, bVecs)
%
%   Estimate diffusion tensory
%
%   sigVals:    [N_bval * Nx * Ny * Nz] matrix
%   bVals:      [N_bval * 1] vector  [s/mm^2]
%   bVecs:      [N_bval * 3] matrix  [s/mm^2]
%
%   (c) 2022, Corey Baron

% Check inputs
szin = [size(sigVals), 1];
if ~isreal(sigVals)
    sigVals = abs(sigVals);
end

% Normalize bvecs
bVecs = bVecs./sqrt(max(sum(bVecs.^2,2)));

% Determine diffusion tensor
y = -log(sigVals);
A = zeros(length(bVals),7);
for n=1:length(bVals)
    bmat = bVals(n)*(bVecs(n,:).'*bVecs(n,:));
    A(n,:) = [bmat(1,1), bmat(2,2), bmat(3,3),...
        2*bmat(1,2), 2*bmat(1,3), 2*bmat(2,3), 1];
end
DT = zeros([7,szin(2:end)], 'like', sigVals);
DT(:,:) = A\y(:,:);

% Determine MD 
MD = reshape(mean(DT(1:3,:),1), szin(2:end));

% Determine eigenvalues
eigVals = zeros([3,szin(2:end)], 'like', sigVals);
for n=1:prod(szin(2:end))
    if any(isnan(DT(:,n)))
        DT(:,n) = 0;
    else
        DT_a = [DT(1,n) DT(4,n) DT(5,n);
            DT(4,n) DT(2,n) DT(6,n);
            DT(5,n) DT(6,n) DT(3,n)];
        eigVals(:,n) = eig(DT_a);
    end
end

% Determine FA
FA = sqrt( 0.5*((eigVals(1,:)-eigVals(2,:)).^2 + ...
    (eigVals(2,:)-eigVals(3,:)).^2 + ...
    (eigVals(1,:)-eigVals(3,:)).^2) ./ ...
    sum(eigVals(:,:).^2,1) );
FA = reshape(FA, szin(2:end));

end

 