# Version information
This is the version used for https://doi.org/10.3389/fnins.2023.1074730 . The newest version is available in the matmri toolbox (https://gitlab.com/cfmm/matlab/matmri).

# Summary
The code in this repository is for performing a free-water elimination algorithm for the fitting of microscopic fractional anisotropy, linear kurtosis, and isotropic kurtosis for data acquired with both linear tensor encoding and spherical tensor encoding.

# Acknowledgements

Cite as: 
- Arezza, N. J. J., Santini, T., Omer, M., & Baron, C. A. (2023). Estimation of free water-corrected microscopic fractional anisotropy. Frontiers in Neuroscience, 17. https://doi.org/10.3389/fnins.2023.1074730

# File descriptions
  - uFA_fwe.m: start with this one. The inputs are paths to:
    - nifti file containing the raw image data
    - file containing b-value information. Can be the *.bval file outputted by dcm2niix when converting dicoms to nifti
    - file containing a list of whether scans were acquired with LTE (value = 0 in file) or STE (value = 1 in file) encoding.  
    - (optional) nifti defining a mask
  - estSigFracLowb.m: uFA_fwe calls this to estimate the signal fraction from low b-value data
  - estKurt.m: uFA_few calls this to perform the full free water elimination fitting
  - estDT.m: not used by uFA_fwe. Used to estimate a diffusion tensor using ordinary least squares. This is included for completeness, but many packages have more robust methods for this (e.g., ExploreDTI or mrtrix3).

(c) 2022, Corey Baron and Nico Arezza
